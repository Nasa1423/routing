Глоссарий:
Магистраль - ребро графа, обладающее собственными характеристиками длины (характеризует время прохождения через магистраль, измеряется у.е.).

Узел сети - вершина графа, через которую может проходить маршрут.

ID узла сети - порядковый номер узла в сети, значение которого принадлежит множеству [0, 9223372036854775807] для всех целых чисел

Постановка задачи:

Имя задачи: Маршрутизация в сети, симулирующей всемирную сеть Интернет

Цель: Разработка алгоритма для построения кратчайшего маршрута между двумя произвольными узлами взвешенного (со значениями весов в целочисленном 
диапазоне [1,3000000] у.е.) ненаправленного графа с производительностью превышающей производительность алгоритма Дейкстры при количестве узлов в 
графе 10^9–10^10 и плотностью ребер – 2-4 на узел с использованием параллелизма процессов.

Входные данные: ID двух выбранных узлов сети и случайно сгенерированный граф

Выходные данные: Последовательный список ID узлов сети, через которые будет проходить оптимальный маршрут.
