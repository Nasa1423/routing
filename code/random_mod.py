import random
import bisect

# Готовый класс из интернета
class WeightedChoice(object):
    def __init__(self, weights):
        self.totals = []
        self.weights = weights
        running_total = 0

        for w in weights:
            running_total += w[1]
            self.totals.append(running_total)

    def next(self):
        rnd = random.random() * self.totals[-1]
        i = bisect.bisect_right(self.totals, rnd)
        return self.weights[i][0]


# Основная функция
def random_custom(sec, fourth):
    arr = [(3, 50)] # Массив с шансами
    chance = 0
    if fourth == 0:
        chance4th = 0.5
    elif sec == 0:
        chance2th = 0.5
    else:
        chance = sec / fourth # Вычисляем пропорцию сгенерированных магистралей
    chance2th = 50 / (chance + 1) * (1/chance)
    chance4th = 50 / (chance + 1) * chance
    #print(chance2th, chance4th)
    arr.append((2, chance2th))
    arr.append((4, chance4th))
    # Записали сгененерированные вероятности в массив и отправили его в класс рандомизации
    choice = WeightedChoice(arr);
    return choice.next()
print(random_custom(2,6)) # По условию: было сгенерированно 2 узла c 2
# магистралями и 6 узлов с 4 магистралями

